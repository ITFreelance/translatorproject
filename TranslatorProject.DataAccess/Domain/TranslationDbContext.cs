﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using TranslatorProject.DataAccess.Entities;

namespace TranslatorProject.DataAccess.Domain
{
    public class TranslationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, int>
    {
        public TranslationDbContext
               (DbContextOptions<TranslationDbContext> options)
               : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<WordTranslation>()
                .HasKey(wt => new { wt.WordId, wt.TranslationId });

            builder.Entity<WordTranslation>()
                        .HasOne(e => e.Word)
                        .WithMany(e => e.DefaultWordTranslations)
                        .HasForeignKey(e => e.WordId)
                        .OnDelete(DeleteBehavior.Cascade);

            builder.Entity<WordTranslation>()
                        .HasOne(e => e.Translation)
                        .WithMany(e => e.WordTranslations)
                        .HasForeignKey(e => e.TranslationId)
                        .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Language>().HasData(
                new Language[]
                {
                    new Language { Id = 1, Name = "Русский" },
                    new Language { Id = 2, Name = "Английский" }
                });

            builder.Entity<WordType>().HasData(
                new WordType[]
                {
                    new WordType { Id = 1, Name = "Существительное" },
                    new WordType { Id = 2, Name = "Местоимение" },
                    new WordType { Id = 3, Name = "Глагол" },
                    new WordType { Id = 4, Name = "Прилагательное" },
                    new WordType { Id = 5, Name = "Числительное" },
                    new WordType { Id = 6, Name = "Наречие" },
                    new WordType { Id = 7, Name = "Предлог" },
                    new WordType { Id = 8, Name = "Артикль" }
                });
        }

        public DbSet<Language> Languages { get; set; }
        public DbSet<WordType> WordTypes { get; set; }

        public DbSet<Word> Words { get; set; }

        public DbSet<WordTranslation> WordTranslations { get; set; }
    }
}
