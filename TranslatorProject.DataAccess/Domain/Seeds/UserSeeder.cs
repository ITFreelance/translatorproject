﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TranslatorProject.Common.Models;
using TranslatorProject.DataAccess.Entities;

namespace TranslatorProject.DataAccess.Domain.Seeds
{
    public class UserSeeder
    {
        private const string AdminLogin = "admin";
        private const string Password = "123qwe";

        public async Task SeedAsync(TranslationDbContext context, IServiceProvider serviceProvider)
        {
            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<ApplicationRole>>();
            await SeedUserRolesAsync(userManager, roleManager, context);
        }

        private static async Task SeedUserRolesAsync(UserManager<ApplicationUser> userManager, RoleManager<ApplicationRole> roleManager, TranslationDbContext context)
        {
            var user = await userManager.FindByNameAsync(AdminLogin);
            if (user == null)
            {
                user = new ApplicationUser()
                {
                    UserName = AdminLogin,
                    Firstname = AdminLogin,
                    Surname = AdminLogin,
                    Patronymic = AdminLogin
                };
                var result = await userManager.CreateAsync(user, Password);
                if (!result.Succeeded) return;
                else
                {
                    await SeedRolesAsync(roleManager);
                    var role = await context.Roles.FirstOrDefaultAsync(x => x.Name == GlobalConstants.Roles.Administrator);
                    if (role != null)
                    {
                        context.UserRoles.Add(new IdentityUserRole<int>
                        {
                            UserId = user.Id,
                            RoleId = role.Id,
                        });
                    }
                }
            }

            await context.SaveChangesAsync();
        }

        private static async Task SeedRolesAsync(RoleManager<ApplicationRole> roleManager)
        {
            var roles = new List<string>
            {
                GlobalConstants.Roles.Administrator,
                GlobalConstants.Roles.User
            };
            foreach (var item in roles)
            {
                ApplicationRole role = new ApplicationRole()
                {
                    Name = item
                };
                await roleManager.CreateAsync(role);                
            }
        }
    }
}
