﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;

namespace TranslatorProject.DataAccess.Entities
{
    public class ApplicationUser : IdentityUser<int>
    {
        [Required, MaxLength(100)]
        public string Firstname { get; set; }
        [Required, MaxLength(100)]
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string FullName => $"{Surname} {Firstname} {Patronymic}";
    }
}
