﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranslatorProject.DataAccess.Entities
{
    [Table("WordTypes")]
    public class WordType
    {
        [Key]
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
