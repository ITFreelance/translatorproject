﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranslatorProject.DataAccess.Entities
{
    [Table("Languages")]
    public class Language
    {
        [Key]
        public byte Id { get; set; }
        public string Name { get; set; }
    }
}
