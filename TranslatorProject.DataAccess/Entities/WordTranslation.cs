﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TranslatorProject.DataAccess.Entities
{
    [Table("WordTranslations")]
    public class WordTranslation
    {
        public int WordId { get; set; }
        [ForeignKey("WordId")]
        public Word Word { get; set; }

        public int TranslationId { get; set; }
        [ForeignKey("TranslationId")]
        public Word Translation { get; set; }
    }
}
