﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace TranslatorProject.DataAccess.Entities
{
    [Table("Words")]
    public class Word
    {
        public Word()
        {
        }

        public Word(string name, byte langId, byte typeId, int id = 0)
        {
            Id = id;
            Name = name;
            LanguageId = langId;
            TypeId = typeId;
        }

        [Key]
        public int Id { get; set; }

        public string Name { get; set; }

        public byte LanguageId { get; set; }
        [ForeignKey("LanguageId")]
        public Language Language { get; set; }

        public byte TypeId { get; set; }
        [ForeignKey("TypeId")]
        public WordType WordType { get; set; }

        public virtual ICollection<WordTranslation> DefaultWordTranslations { get; set; }
        public virtual ICollection<WordTranslation> WordTranslations { get; set; }

        public string ToJson()
        {
            return JsonSerializer.Serialize(this);
        }
    }
}
