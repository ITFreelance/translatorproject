﻿using System.Collections.Generic;
using System.Linq;

namespace TranslatorProject.Common.Models
{
    public class Result
    {
        public Result(bool succeeded, string type, string message)
        {
            IsSuccess = succeeded;
            AlertType = type;
            AlertMessage = message;
        }

        public Result(bool succeeded, IEnumerable<string> errors)
        {
            IsSuccess = succeeded;
            Errors = errors.ToArray();
        }

        public bool IsSuccess { get; set; }
        public string AlertType { get; set; }
        public string AlertMessage { get; set; }
        public string[] Errors { get; set; }

        public static Result Success()
        {
            return new Result(true, new string[] { });
        }

        public static Result Failure(IEnumerable<string> errors)
        {
            return new Result(false, errors);
        }

        public static Result AlertSuccess(string message)
        {
            return new Result(true, "success", message);
        }

        public static Result AlertFailure(string message)
        {
            return new Result(false, "danger", message);
        }
    }
}
