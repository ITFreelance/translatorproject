﻿namespace TranslatorProject.Common.Models
{
    public static class GlobalConstants
    {
        public static class Roles
        {
            public const string Administrator = "Administrator";

            public const string User = "User";
        }
    }
}
