﻿using System.ComponentModel.DataAnnotations;

namespace TranslatorProject.Common.Enums
{
    public enum EnumLanguage : byte
    {
        [Display(Name = "Русский")]
        Russian = 1,
        [Display(Name = "Английский")]
        English = 2
    }
}
