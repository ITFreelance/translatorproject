﻿using Microsoft.Extensions.DependencyInjection;
using TranslatorProject.Common.Interfaces;
using TranslatorProject.Common.Services;

namespace TranslatorProject.Common.Utils
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommon(this IServiceCollection services)
        {
            services.AddTransient<ICurrentUserService, CurrentUserService>();

            return services;
        }
    }
}
