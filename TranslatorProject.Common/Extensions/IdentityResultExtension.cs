﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using TranslatorProject.Common.Models;

namespace TranslatorProject.Common.Extensions
{
    public static class IdentityResultExtension
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }

        public static Result ToApplicationResult(this SignInResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(new List<string>() {
                    "Неправильный логин и (или) пароль",
                    "Пользователь не найден, либо не существует!"
                });
        }
    }
}
