﻿using Microsoft.AspNetCore.Http;
using System;
using System.Security.Claims;
using TranslatorProject.Common.Interfaces;
using TranslatorProject.Common.Models;

namespace TranslatorProject.Common.Services
{
    public class CurrentUserService : ICurrentUserService
    {
        public CurrentUserService(IHttpContextAccessor httpContextAccessor)
        {
            UserId = Convert.ToInt32(httpContextAccessor.HttpContext?.User?.FindFirstValue(ClaimTypes.NameIdentifier));
            IsAdmin = httpContextAccessor.HttpContext?.User?.IsInRole(GlobalConstants.Roles.Administrator);
        }

        public int UserId { get; }
        public bool? IsAdmin { get; }
    }
}
