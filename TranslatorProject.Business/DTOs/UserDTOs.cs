﻿namespace TranslatorProject.Business.DTOs
{
    public class UserInsertDTO
    {
        public string UserName { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }
        public string Password { get; set; }
    }

    public class UserListDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
    }
}
