﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranslatorProject.Business.DTOs
{
    public class TranslationWordInsertDTO
    {
        public string WordText { get; set; }
        public byte LangId { get; set; }
        public string TranslationWordText { get; set; }
        public byte TranslationLangId { get; set; }
        public byte WordType { get; set; }
    }

    public class TranslationWordUpdateDTO
    {
        public int WordId { get; set; }
        public string WordText { get; set; }
        public byte LangId { get; set; }

        public int TranslationWordId { get; set; }
        public string TranslationWordText { get; set; }
        public byte TranslationLangId { get; set; }

        public byte TypeId { get; set; }
    }

    public class TranslatorWordDTO
    {
        public int WordId { get; set; }
        public string Text { get; set; }
        public string TranslationText { get; set; }
        public string TypeName { get; set; }
    }

    public class TranslatorWordFilterDTO
    {
        public byte WordType { get; set; }
        public string WordText { get; set; }
        public string SortColumn { get; set; }
        public bool SortColumnDir { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
