﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TranslatorProject.Business.DTOs
{
    public class LanguageDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class WordTypeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
