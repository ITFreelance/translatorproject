﻿using System.Collections.Generic;
using TranslatorProject.Business.DTOs;

namespace TranslatorProject.Business.Interfaces
{
    public interface ITranslatorService
    {
        List<TranslatorWordDTO> GetTranslatorList(int pageIndex, byte langId, string word, int pageSize = 20);
    }
}
