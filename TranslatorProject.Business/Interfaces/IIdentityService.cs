﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Common.Models;

namespace TranslatorProject.Business.Interfaces
{
    public interface IIdentityService
    {
        Task<Result> SignInAsync(string userName, string password);
        Task SignOutAsync();

        Task<List<UserListDTO>> GetListUsersAsync();
        Task<string> GetUserNameAsync(int userId);
        Task<(Result Result, int UserId)> CreateUserAsync(UserInsertDTO userInsert);
        Task<Result> DeleteUserAsync(int userId);
    }
}
