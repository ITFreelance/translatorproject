﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Common.Models;

namespace TranslatorProject.Business.Interfaces
{
    public interface IWordTranslationService
    {
        Task<(List<TranslatorWordDTO>, int)> GetWordsDataForTable(TranslatorWordFilterDTO dto);
        Task<TranslationWordUpdateDTO> GetDetailsTranslationWordAsync(int wordId);
        Task<Result> InsertTranslationWordAsync(TranslationWordInsertDTO dto);
        Task<Result> UpdateTranslationWordAsync(TranslationWordUpdateDTO dto); 
        Task<Result> DeleteTranslationWordAsync(int wordId);
    }
}
