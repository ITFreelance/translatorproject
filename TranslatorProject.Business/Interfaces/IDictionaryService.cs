﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;

namespace TranslatorProject.Business.Interfaces
{
    public interface IDictionaryService
    {
        Task<List<WordTypeDTO>> GetTypesListAsync();
        Task<List<LanguageDTO>> GetLangsListAsync(byte id);
    }
}
