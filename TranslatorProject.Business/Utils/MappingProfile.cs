﻿using AutoMapper;
using TranslatorProject.Business.DTOs;
using TranslatorProject.DataAccess.Entities;

namespace TranslatorProject.Business.Utils
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserInsertDTO, ApplicationUser>();
            CreateMap<ApplicationUser, UserListDTO>();

            CreateMap<Language, LanguageDTO>();
            CreateMap<WordType, WordTypeDTO>();
        }
    }
}
