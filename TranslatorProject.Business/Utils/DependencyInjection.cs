﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Pomelo.EntityFrameworkCore.MySql.Storage;
using System;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.Business.Services;
using TranslatorProject.DataAccess.Domain;
using TranslatorProject.DataAccess.Domain.Seeds;
using TranslatorProject.DataAccess.Entities;
using TranslatorProject.DataAccess.Interfaces;
using TranslatorProject.DataAccess.Repositories;

namespace TranslatorProject.Business.Utils
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddBusiness(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddIdentity<ApplicationUser, ApplicationRole>
                (options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequiredLength = 6;
                })
                .AddEntityFrameworkStores<TranslationDbContext>()
                .AddDefaultTokenProviders();

            /*Данная конфигурация используется для SQL Server*/
            //services.AddDbContext<TranslationDbContext>(options =>
            //    options.UseSqlServer(
            //        configuration.GetConnectionString("TranslationConnectionSqlServer"),
            //        b => b.MigrationsAssembly(typeof(TranslationDbContext).Assembly.FullName)));

            /*Данная конфигурация используется для MYSQL*/
            services.AddDbContextPool<TranslationDbContext>(options =>
                options.UseMySql(
                    configuration.GetConnectionString("TranslationConnectionMysql"), mySqlOptions => mySqlOptions
                    .ServerVersion(new ServerVersion(new Version(8, 0, 18), ServerType.MySql))));

            services.AddTransient(typeof(IGenericRepo<>), typeof(GenericRepo<>));
            services.AddTransient<IIdentityService, IdentityService>();
            services.AddTransient<IWordTranslationService, WordTranslationService>();
            services.AddTransient<ITranslatorService, TranslatorService>();
            services.AddTransient<IDictionaryService, DictionaryService>();

            return services;
        }

        public static void AddBusinessAppBuilder(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var dbContext = serviceScope.ServiceProvider.GetRequiredService<TranslationDbContext>();
                dbContext.Database.Migrate();

                new UserSeeder().SeedAsync(dbContext, serviceScope.ServiceProvider).GetAwaiter().GetResult();
            }
        }
    }
}
