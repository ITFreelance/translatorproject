﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.Common.Extensions;
using TranslatorProject.Common.Models;
using TranslatorProject.DataAccess.Entities;

namespace TranslatorProject.Business.Services
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly IMemoryCache _memoryCache;
        private readonly IMapper _mapper;

        public IdentityService
            (UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager,
            IMemoryCache memoryCache, IMapper mapper)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _memoryCache = memoryCache;
            _mapper = mapper;
        }

        public async Task<Result> SignInAsync(string userName, string password)
        {
            var result = await _signInManager.PasswordSignInAsync(userName, password, false, false);
            return result.ToApplicationResult();
        }

        public async Task SignOutAsync()
        {
            _memoryCache.ClearCache();
            await _signInManager.SignOutAsync();
        }

        public async Task<List<UserListDTO>> GetListUsersAsync()
        {
            var userList = await _userManager.Users.ToListAsync();
            return _mapper.Map<List<UserListDTO>>(userList);
        }

        public async Task<string> GetUserNameAsync(int userId)
        {
            var user = await _userManager.Users.FirstAsync(u => u.Id == userId);
            return user.UserName;
        }

        public async Task<(Result Result, int UserId)> CreateUserAsync(UserInsertDTO userInsert)
        {
            ApplicationUser user = _mapper.Map<ApplicationUser>(userInsert);
            var result = await _userManager.CreateAsync(user, userInsert.Password);
            await _userManager.AddToRoleAsync(user, GlobalConstants.Roles.User);

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<Result> DeleteUserAsync(int userId)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);
            if (user != null)
                return await DeleteUserAsync(user);

            return Result.Failure(new List<string>() { "Пользователь не найден, либо не существует!" });
        }

        private async Task<Result> DeleteUserAsync(ApplicationUser user)
        {
            var result = await _userManager.DeleteAsync(user);
            return result.ToApplicationResult();
        }
    }
}
