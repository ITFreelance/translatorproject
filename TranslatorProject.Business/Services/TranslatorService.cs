﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.DataAccess.Entities;
using TranslatorProject.DataAccess.Interfaces;

namespace TranslatorProject.Business.Services
{
    public class TranslatorService : ITranslatorService
    {
        private readonly IGenericRepo<WordTranslation> _wordTranslationRepo;

        public TranslatorService
            (IGenericRepo<WordTranslation> wordTranslationRepo)
        {
            _wordTranslationRepo = wordTranslationRepo;
        }

        public List<TranslatorWordDTO> GetTranslatorList(int pageIndex, byte langId, string wordName, int pageSize)
        {
            Expression<Func<WordTranslation, bool>> filter = null;

            if (!string.IsNullOrEmpty(wordName))
                filter = word
                    => (word.Translation.LanguageId == langId && word.Word.Name.ToUpper() == wordName.ToUpper())
                    || (word.Word.LanguageId == langId && word.Translation.Name.ToUpper() == wordName.ToUpper());

            var result = _wordTranslationRepo.Filter(
                filter: filter,
                page: pageIndex,
                pageSize: pageSize)
                .Select(x => new TranslatorWordDTO
                {
                    Text = x.Word.Name,
                    TranslationText = x.Translation.Name,
                    TypeName = x.Word.WordType.Name
                }).ToList();

            return result;
        }
    }
}
