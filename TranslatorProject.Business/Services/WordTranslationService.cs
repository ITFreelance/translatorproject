﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.Common.Extensions;
using TranslatorProject.Common.Models;
using TranslatorProject.DataAccess.Entities;
using TranslatorProject.DataAccess.Interfaces;

namespace TranslatorProject.Business.Services
{
    public class WordTranslationService : IWordTranslationService
    {
        private readonly IGenericRepo<Word> _wordRepo;
        private readonly IGenericRepo<WordTranslation> _wordTranslationRepo;

        public WordTranslationService
            (IGenericRepo<Word> wordRepo, IGenericRepo<WordTranslation> wordTranslationRepo)
        {
            _wordRepo = wordRepo;
            _wordTranslationRepo = wordTranslationRepo;
        }

        public async Task<(List<TranslatorWordDTO>, int)> GetWordsDataForTable(TranslatorWordFilterDTO dto)
        {
            var data = _wordTranslationRepo.Query();

            if (dto.WordType != 0)
                data = data.Where(x => x.Word.TypeId == dto.WordType);
            if (!string.IsNullOrEmpty(dto.WordText))
                data = data.Where(x => x.Word.Name.Contains(dto.WordText));

            int recordsTotal = await data.CountAsync();
            var resultData = data
                .Select(x => new TranslatorWordDTO
                {
                    WordId = x.WordId,
                    Text = x.Word.Name,
                    TranslationText = x.Translation.Name,
                    TypeName = x.Word.WordType.Name
                })
                .OrderByDynamic(dto.SortColumn, dto.SortColumnDir)
                .Skip(dto.PageIndex).Take(dto.PageSize).ToList();

            return (resultData, recordsTotal);
        }

        public async Task<Result> InsertTranslationWordAsync(TranslationWordInsertDTO dto)
        {
            Word word = new Word(dto.WordText, dto.LangId, dto.WordType);
            if (await _wordRepo.AddAsync(word) == null)
                return Result.AlertFailure("Ошибка: процедура не выполнилась");

            Word translationWord = new Word(dto.TranslationWordText, dto.TranslationLangId, dto.WordType);
            if (await _wordRepo.AddAsync(translationWord) == null)
                return Result.AlertFailure("Ошибка: процедура не выполнилась");

            WordTranslation wt = new WordTranslation
            {
                WordId = word.Id,
                TranslationId = translationWord.Id
            };
            if (await _wordTranslationRepo.AddAsync(wt) == null)
                return Result.AlertFailure("Ошибка: процедура не выполнилась");

            return Result.AlertSuccess("Успешно: процедура выполнилась");
        }

        public async Task<TranslationWordUpdateDTO> GetDetailsTranslationWordAsync(int wordId)
        {
            var translationWord = await _wordTranslationRepo.Query()
              .Include(i => i.Word)
              .Include(i => i.Translation)
              .Select(wt => new TranslationWordUpdateDTO
              {
                  WordId = wt.WordId,
                  WordText = wt.Word.Name,
                  LangId = wt.Word.LanguageId,
                  TranslationWordId = wt.TranslationId,
                  TranslationWordText = wt.Translation.Name,
                  TranslationLangId = wt.Translation.LanguageId,
                  TypeId = wt.Word.TypeId
              })
              .SingleOrDefaultAsync(x => x.WordId == wordId);

            return translationWord;
        }

        public async Task<Result> UpdateTranslationWordAsync(TranslationWordUpdateDTO dto)
        {
            Word word = new Word(dto.WordText, dto.LangId, dto.TypeId, dto.WordId);
            if (await _wordRepo.UpdateAsync(word) == null)
                return Result.AlertFailure("Ошибка: процедура не выполнилась");

            Word translationWord = new Word(dto.TranslationWordText, dto.TranslationLangId, dto.TypeId, dto.TranslationWordId);
            if (await _wordRepo.UpdateAsync(translationWord) == null)
                return Result.AlertFailure("Ошибка: процедура не выполнилась");

            return Result.AlertSuccess("Успешно: процедура выполнилась");
        }
        
        public async Task<Result> DeleteTranslationWordAsync(int wordId)
        {
            var wordTranslation = await _wordTranslationRepo.FindByFilterAsync(x => x.WordId == wordId);

            await _wordRepo.RemoveAsync(_wordRepo.FindById(wordTranslation.WordId));
            await _wordRepo.RemoveAsync(_wordRepo.FindById(wordTranslation.TranslationId));

            return Result.AlertSuccess("Успешно: процедура выполнилась");
        }
    }
}
