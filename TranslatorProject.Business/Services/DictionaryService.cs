﻿using AutoMapper;
using System.Collections.Generic;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.DataAccess.Entities;
using TranslatorProject.DataAccess.Interfaces;

namespace TranslatorProject.Business.Services
{
    public class DictionaryService : IDictionaryService
    {
        private readonly IGenericRepo<Language> _langRepo;
        private readonly IGenericRepo<WordType> _typeRepo;
        private readonly IMapper _mapper;

        public DictionaryService
            (IGenericRepo<Language> langRepo, IGenericRepo<WordType> typeRepo, IMapper mapper)
        {
            _langRepo = langRepo;
            _typeRepo = typeRepo;
            _mapper = mapper;
        }

        public async Task<List<WordTypeDTO>> GetTypesListAsync()
        {
            return _mapper.Map<List<WordTypeDTO>>(await _typeRepo.GetAllAsync());
        }

        public async Task<List<LanguageDTO>> GetLangsListAsync(byte id)
        {
            return _mapper.Map<List<LanguageDTO>>(await _langRepo.GetAllFindByAsync(x => x.Id == id));
        }
    }
}
