﻿using AutoMapper;
using TranslatorProject.Business.DTOs;
using TranslatorProject.WebUI.ViewModels;

namespace TranslatorProject.WebUI.Utils
{
    public partial class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<UserCreateViewModel, UserInsertDTO>();
            CreateMap<UserListDTO, UserIndexViewModel>();

            CreateMap<WordTranslationCreateViewModel, TranslationWordInsertDTO>();
            CreateMap<WordTranslationEditViewModel, TranslationWordUpdateDTO>()
                .ForMember(dest => dest.WordText, dest => dest.MapFrom(item => item.WordTextEdit))
                .ForMember(dest => dest.TranslationWordText, dest => dest.MapFrom(item => item.TranslationWordTextEdit))
                .ForMember(dest => dest.LangId, dest => dest.MapFrom(item => item.LangIdEdit))
                .ForMember(dest => dest.TranslationLangId, dest => dest.MapFrom(item => item.TranslationLangIdEdit));
        }
    }
}
