﻿var pageIndex = 0;

$(document).ready(function () {
    GetData();

    $(window).scroll(function () {
        if ($(window).scrollTop() == $(document).height() - $(window).height()) {
            var text = $('#txtWordText').val().trim();
            var langId = $('#ddlType').val().trim();
            GetData(text, langId);
        }
    });
});

function GetData(text, langId) {
    $.ajax({
        type: 'GET',
        url: '/Translator/LoadData',
        data:
        {
            "pageIndex": pageIndex,
            "wordName": text,
            "langId": langId
        },
        dataType: 'json',
        success: function (data) {
            if (data != null) {
                for (var i = 0; i < data.length; i++) {
                    $('#table tbody').append('<tr class="child"><td>'
                        + data[i].text + '</td><td>'
                        + data[i].translationText + '</td><td>'
                        + data[i].typeName + '</td></tr>');
                }
                pageIndex++;
            }
        },
        beforeSend: function () {
            $("#progress").show();
        },
        complete: function () {
            $("#progress").hide();
        },
        error: function () {
            alert("Ошибка при выведении данных");
        }
    });
}

$('#btnSearch').click(function () {
    var text = $('#txtWordText').val().trim();
    var langId = $('#ddlType').val().trim();
    $("#table > tbody").html("");
    pageIndex = 0;
    GetData(text, langId);
});