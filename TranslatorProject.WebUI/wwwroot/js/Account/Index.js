﻿var table;
$(document).ready(function () {
    table = $('.table').DataTable({
        "ajax": {
            "url": "/Account/LoadData",
            "type": "GET",
            "datatype": "json",
            "dataSrc": ""
        },
        "columns": [
            {
                "render": function (data, type, full, meta) {
                    let count = meta.row;
                    count = count + 1;
                    totalCount = count;
                    return count;
                }
            },
            { "data": "userName" },
            { "data": "fullName" }
        ],
        "language": {
            "url": '/datatables.Russian.json'
        }
    });
});

$('#create-modal').on('hidden.bs.modal', function (e) {
    $(this).find('form')[0].reset();
});
createSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#create-modal").modal("hide");
};
createError = function (alert) {
    showAlert(alert.responseJSON);
    $("#create-modal").modal("hide");
};