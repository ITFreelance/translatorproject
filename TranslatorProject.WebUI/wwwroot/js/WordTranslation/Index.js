﻿var table;
$(document).ready(function () {
    table = $(".table").DataTable({
        "processing": true,
        "serverSide": true,
        "orderMulti": false,
        "dom": '<"top"i>rt<"bottom"lp><"clear">',
        "ajax": {
            "url": "/WordTranslation/LoadData",
            "type": "POST",
            "datatype": "json"
        },
        "language": {
            "url": '/datatables.Russian.json'
        },
        "columns": [
            { "data": "text", "name": "text", "autoWidth": true },
            { "data": "translationText", "name": "translationText", "autoWidth": true },
            { "data": "typeName", "name": "typeName", "autoWidth": true },
            {
                "data": "wordId",
                "searchable": false,
                "sortable": false,
                "render": function (data, type, full, meta) {
                    return '<input type="button" class="btn btn-sm btn-success mr-2" onclick="Edit(' + full.wordId + ')" value="Изменить"/>' +
                        '<input type="button" class="btn btn-sm btn-danger" onclick="Delete(' + full.wordId + ')" value="Удалить"/>';
                }
            }
        ]
    });

    oTable = $('.table').DataTable();
    $('#btnSearch').click(function () {
        oTable.columns(0).search($('#txtWordText').val().trim());
        oTable.columns(2).search($('#ddlType').val().trim());
        oTable.draw();
    });
});

$('#create-modal').on('hidden.bs.modal', function (e) {
    $(this).find('form')[0].reset();
});
createSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#create-modal").modal("hide");
};
createError = function (alert) {
    showAlert(alert.responseJSON);
    $("#create-modal").modal("hide");
};


function Edit(id) {
    var url = "/WordTranslation/Details?wordId=" + id;
    $.ajax({
        type: "GET",
        url: url,
        success: function (data) {
            console.log(data);
            $('#WordId').val(data.wordId);
            $('#LangIdEdit').val(data.langId).change();
            $('#WordTextEdit').val(data.wordText);

            $('#TranslationWordId').val(data.translationWordId);
            $('#TranslationLangIdEdit').val(data.translationLangId).change();
            $('#TranslationWordTextEdit').val(data.translationWordText);

            $('#TypeId').val(data.typeId).change();
            $("#edit-modal").modal("show");
        }
    });
}
editSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#edit-modal").modal("hide");
};
editError = function (alert) {
    showAlert(alert.responseJSON);
    $("#edit-modal").modal("hide");
};


function Delete(id) {
    $('#DeleteWordId').val(id);
    $("#delete-modal").modal("show");
}
deleteSuccess = function (alert) {
    showAlert(alert);
    table.ajax.reload(null, false);
    $("#delete-modal").modal("hide");
};