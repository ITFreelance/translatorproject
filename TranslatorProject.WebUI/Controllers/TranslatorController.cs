﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TranslatorProject.Business.Interfaces;

namespace TranslatorProject.WebUI.Controllers
{
    [Authorize]
    public class TranslatorController : Controller
    {
        private readonly ITranslatorService _service;

        public TranslatorController
            (ITranslatorService service)
        {
            _service = service;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult LoadData(int pageIndex, byte langId, string wordName)
        {
            var query = _service.GetTranslatorList(pageIndex, langId, wordName);
            return Json(query);
        }
    }
}