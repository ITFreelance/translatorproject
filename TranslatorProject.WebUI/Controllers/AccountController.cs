﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.Common.Models;
using TranslatorProject.WebUI.ViewModels;

namespace TranslatorProject.WebUI.Controllers
{
    [Authorize(Roles = GlobalConstants.Roles.Administrator)]
    public class AccountController : Controller
    {
        private readonly IIdentityService _identityService;
        private readonly IMapper _mapper;

        public AccountController
            (IIdentityService identityService, IMapper mapper)
        {
            _identityService = identityService;
            _mapper = mapper;
        }

        public IActionResult Index() => View();

        public async Task<IActionResult> LoadData()
        {
            List<UserListDTO> result = await _identityService.GetListUsersAsync();
            return Ok(_mapper.Map<List<UserIndexViewModel>>(result));
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserCreateViewModel model)
        {
            UserInsertDTO userInsert = _mapper.Map<UserInsertDTO>(model);
            var result = await _identityService.CreateUserAsync(userInsert);

            if (result.Result.IsSuccess)
                return Ok(Result.AlertSuccess("Успешно: процедура выполнилась"));
            else
                return BadRequest(Result.AlertSuccess("Ошибка: процедура не выполнилась"));
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login(string returnUrl = null)
        {
            if(User.Identity.IsAuthenticated)
                return Redirect("/Home/Index");
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await _identityService.SignInAsync(model.UserName, model.Password);
                if (result.IsSuccess)
                {
                    if (!string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl))
                        return Redirect(model.ReturnUrl);
                    else
                        return Redirect("/Home/Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                        ModelState.AddModelError("", error);
                }
            }
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> LogOff()
        {
            await _identityService.SignOutAsync();
            return RedirectToAction("Login");
        }
    }
}