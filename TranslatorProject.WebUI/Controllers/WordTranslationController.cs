﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;
using TranslatorProject.Business.DTOs;
using TranslatorProject.Business.Interfaces;
using TranslatorProject.Common.Enums;
using TranslatorProject.Common.Models;
using TranslatorProject.WebUI.ViewModels;

namespace TranslatorProject.WebUI.Controllers
{
    [Authorize(Roles = GlobalConstants.Roles.Administrator)]
    public class WordTranslationController : Controller
    {
        private readonly IWordTranslationService _service;
        private readonly IDictionaryService _dictService;
        private readonly IMapper _mapper;

        public WordTranslationController
            (IWordTranslationService service, IDictionaryService dictService, IMapper mapper)
        {
            _service = service;
            _dictService = dictService;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.Langs = new SelectList(await _dictService.GetLangsListAsync((byte)EnumLanguage.Russian), "Id", "Name");
            ViewBag.TranslationLangs = new SelectList(await _dictService.GetLangsListAsync((byte)EnumLanguage.English), "Id", "Name");
            ViewBag.Types = new SelectList(await _dictService.GetTypesListAsync(), "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LoadData()
        {
            var dataTableDict = Request.Form.ToDictionary(x => x.Key, x => x.Value.ToString());
            var draw = dataTableDict["draw"];

            TranslatorWordFilterDTO filterDTO = new TranslatorWordFilterDTO();
            filterDTO.SortColumn = dataTableDict["columns[" + dataTableDict["order[0][column]"] + "][name]"];
            filterDTO.SortColumnDir = dataTableDict["order[0][dir]"].Contains("asc");
            filterDTO.WordText = dataTableDict["columns[0][search][value]"];
            filterDTO.WordType = !string.IsNullOrEmpty(dataTableDict["columns[2][search][value]"]) ? Convert.ToByte(dataTableDict["columns[2][search][value]"]) : (byte)0;
            filterDTO.PageSize = !string.IsNullOrEmpty(dataTableDict["length"]) ? Convert.ToInt32(dataTableDict["length"]) : 0;
            filterDTO.PageIndex = !string.IsNullOrEmpty(dataTableDict["start"]) ? Convert.ToInt32(dataTableDict["start"]) : 0;

            var result = await _service.GetWordsDataForTable(filterDTO);
            return Json(new { draw, recordsFiltered = result.Item2, recordsTotal = result.Item2, data = result.Item1 });
        }

        [HttpPost]
        public async Task<IActionResult> Create(WordTranslationCreateViewModel model)
        {
            var dto = _mapper.Map<TranslationWordInsertDTO>(model);
            var result = await _service.InsertTranslationWordAsync(dto);
            if (result.IsSuccess)
                return Ok(result);
            else
                return BadRequest(result);
        }

        public async Task<IActionResult> Details(int wordId) => Ok(await _service.GetDetailsTranslationWordAsync(wordId));

        [HttpPost]
        public async Task<IActionResult> Edit(WordTranslationEditViewModel model)
        {
            var dto = _mapper.Map<TranslationWordUpdateDTO>(model);
            var result = await _service.UpdateTranslationWordAsync(dto);
            if (result.IsSuccess)
                return Ok(result);
            else
                return BadRequest(result);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(int deleteWordId) => Ok(await _service.DeleteTranslationWordAsync(deleteWordId));
    }
}