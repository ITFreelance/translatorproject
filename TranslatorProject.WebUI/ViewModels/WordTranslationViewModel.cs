﻿using System.ComponentModel.DataAnnotations;

namespace TranslatorProject.WebUI.ViewModels
{
    public class WordTranslationCreateViewModel
    {
        [Required(ErrorMessage = "Заполните поле {0}")]
        [RegularExpression("^[A-Za-zА-Яа-я]*$", ErrorMessage = "Неверный формат: {0}")]
        [Display(Name = "Слово")]
        public string WordText { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Язык слова")]
        public byte LangId { get; set; }

        [Required(ErrorMessage = "Заполните поле {0}")]
        [RegularExpression("^[A-Za-zА-Яа-я]*$", ErrorMessage = "Неверный формат: {0}")]
        [Display(Name = "Перевод")]
        public string TranslationWordText { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Язык перевода")]
        public byte TranslationLangId { get; set; }

        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Часть речи")]
        public byte WordType { get; set; }
    }

    public class WordTranslationEditViewModel
    {
        public int WordId { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [RegularExpression("^[A-Za-zА-Яа-я]*$", ErrorMessage = "Неверный формат: {0}")]
        [Display(Name = "Слово")]
        public string WordTextEdit { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Язык слова")]
        public byte LangIdEdit { get; set; }


        public int TranslationWordId { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [RegularExpression("^[A-Za-zА-Яа-я]*$", ErrorMessage = "Неверный формат: {0}")]
        [Display(Name = "Перевод")]
        public string TranslationWordTextEdit { get; set; }
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Язык перевода")]
        public byte TranslationLangIdEdit { get; set; }


        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Часть речи")]
        public byte TypeId { get; set; }
    }
}
