﻿using System.ComponentModel.DataAnnotations;

namespace TranslatorProject.WebUI.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "Заполните поле {0}")]
        [Display(Name = "Логин")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Заполните поле {0}")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        public string ReturnUrl { get; set; }
    }

    public class UserIndexViewModel
    {
        [Display(Name = "№ п/п")]
        public int Id { get; set; }
        [Display(Name = "Логин")]
        public string UserName { get; set; }
        [Display(Name = "ФИО пользователя")]
        public string FullName { get; set; }
    }

    public class UserCreateViewModel
    {
        [Display(Name = "Логин")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string UserName { get; set; }
        [Display(Name = "Имя")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Firstname { get; set; }
        [Display(Name = "Фамилия")]
        [Required(ErrorMessage = "Заполните поле {0}")]
        public string Surname { get; set; }
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Required(ErrorMessage = "Заполните поле {0}")]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }
    }
}
